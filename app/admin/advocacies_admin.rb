Trestle.resource(:advocacies) do
  active_storage_fields do
    [:document]
  end

  menu do
    item :advocacies, icon: "fa fa-star"
  end

  table do
    column :pole
    column :name
    column :description
    actions
  end

  form do |article|
    select :pole_id, Pole.order(:name)
    text_field :name
    text_area :description
    active_storage_field :document
  end
end

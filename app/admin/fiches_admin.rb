Trestle.resource(:fiches, model: Fiche) do
  active_storage_fields do
    [:document_1, :document_2, :document_3]
  end

  menu do
    item :fiches, icon: "fa fa-star"
  end

  # Customize the table columns shown on the index view.
  #
  table do
    column :pole
    column :location
    column :country, align: :center
    column :latitude, align: :center
    column :longitude, align: :center
    actions
  end

  # Customize the form fields shown on the new/edit views.
  #
  form do |camp|
    select :pole_id, Pole.order(:name)
    text_field :location
    text_field :country
    text_field :latitude
    text_field :longitude
    editor :description

    text_field :document_1_name
    active_storage_field :document_1

    text_field :document_2_name
    active_storage_field :document_2

    text_field :document_3_name
    active_storage_field :document_3
  end

  # By default, all parameters passed to the update and create actions will be
  # permitted. If you do not have full trust in your users, you should explicitly
  # define the list of permitted parameters.
  #
  # For further information, see the Rails documentation on Strong Parameters:
  #   http://guides.rubyonrails.org/action_controller_overview.html#strong-parameters
  #
  # params do |params|
  #   params.require(:camp).permit(:name, ...)
  # end
end

Trestle.resource(:posts) do
  active_storage_fields do
    [:document_1, :document_2, :document_3]
  end

  menu do
    item :posts, icon: "fa fa-star"
  end

  table do
    column :name
    column :for
    actions
  end

  form do |article|
    text_field :name
    select :for, collection: [:obs, :camps, :blog]
    editor :content

    text_field :document_1_name
    active_storage_field :document_1

    text_field :document_2_name
    active_storage_field :document_2

    text_field :document_3_name
    active_storage_field :document_3
  end
end

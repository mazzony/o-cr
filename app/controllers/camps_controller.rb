class CampsController < ApplicationController
  skip_before_action :verify_authenticity_token

  def create
    Camp.create!(params[:camp].permit!)

    respond_to do |format|
      format.js
    end
  end
end

class FichesController < ApplicationController
  skip_before_action :verify_authenticity_token

  def index
    @fiches = Fiche.pluck(:location, :latitude, :longitude)
    render(json: @fiches)
  end

  def show
    @fiche = Fiche.find(params[:id])
  end
end

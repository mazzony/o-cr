class PagesController < ApplicationController
  def search
    @q = Fiche.search(params[:q])
    @fiches = @q.result
  end

  def show
    if params[:id] == 'search'
      @q = Fiche.search(params[:q])
      @fiches = @q.result
      return render('search')
    end

    @page = Page.find_by(slug: params[:id])

    if !@page
      return render(params[:id])
    end
  end
end

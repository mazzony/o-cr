class PolesController < ApplicationController
  skip_before_action :verify_authenticity_token

  def show
    @pole = Pole.find(params[:id])
  end
end

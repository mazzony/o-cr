module ApplicationHelper
  def google_map(center, size = "300x300")
    "https://maps.googleapis.com/maps/api/staticmap?center=#{center}&size=#{}&zoom=17"
  end
end

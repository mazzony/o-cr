class Advocacy < ApplicationRecord
  belongs_to :pole
  has_one_attached :document
end

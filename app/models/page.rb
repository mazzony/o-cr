class Page < ApplicationRecord
  has_one_attached :document_1
  has_one_attached :document_2
  has_one_attached :document_3
end

class Pole < ApplicationRecord
  has_many :fiches, dependent: :destroy, class_name: 'Fiche'
  has_many :reports, dependent: :destroy
  has_many :advocacies, dependent: :destroy

  has_one_attached :document_1
  has_one_attached :document_2
  has_one_attached :document_3
end

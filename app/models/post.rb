class Post < ApplicationRecord
  enum for: %w(obs camps blog)

  has_one_attached :document_1
  has_one_attached :document_2
  has_one_attached :document_3
end

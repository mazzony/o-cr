class Report < ApplicationRecord
  belongs_to :pole
  has_one_attached :document
end

Rails.application.routes.draw do
  root 'home#index'

  resources :poles
  resources :fiches
  resources :pages
  resources :camps, only: :create
end

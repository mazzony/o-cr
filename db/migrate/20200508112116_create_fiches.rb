class CreateFiches < ActiveRecord::Migration[6.0]
  def change
    create_table :fiches do |t|
      t.string :name
      t.string :location
      t.float :latitude
      t.float :longitude
      t.references :pole, null: false, foreign_key: true

      t.timestamps
    end
  end
end

class AddCountryToFiche < ActiveRecord::Migration[6.0]
  def change
    add_column :fiches, :country, :string
  end
end

class AddDescriptionToPole < ActiveRecord::Migration[6.0]
  def change
    add_column :poles, :description, :text
  end
end

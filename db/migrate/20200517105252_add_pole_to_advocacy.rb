class AddPoleToAdvocacy < ActiveRecord::Migration[6.0]
  def change
    add_reference :advocacies, :pole, null: false, foreign_key: true
  end
end

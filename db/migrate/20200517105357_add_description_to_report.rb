class AddDescriptionToReport < ActiveRecord::Migration[6.0]
  def change
    add_column :reports, :description, :text
    add_column :advocacies, :description, :text
  end
end

class AddDescriptionToFiche < ActiveRecord::Migration[6.0]
  def change
    add_column :fiches, :description, :text
  end
end

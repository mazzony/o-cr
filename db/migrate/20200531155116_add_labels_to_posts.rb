class AddLabelsToPosts < ActiveRecord::Migration[6.0]
  def change
    add_column :posts, :document_1_name, :string
    add_column :posts, :document_2_name, :string
    add_column :posts, :document_3_name, :string

    add_column :poles, :document_1_name, :string
    add_column :poles, :document_2_name, :string
    add_column :poles, :document_3_name, :string

    add_column :pages, :document_1_name, :string
    add_column :pages, :document_2_name, :string
    add_column :pages, :document_3_name, :string

    add_column :fiches, :document_1_name, :string
    add_column :fiches, :document_2_name, :string
    add_column :fiches, :document_3_name, :string
  end
end

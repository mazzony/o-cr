# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Pole.destroy_all
# Post.destroy_all

# pole = Pole.create!(name: "Pôle Afrique du Nord et Moyen-Orient")

# [
#   ["Bourj El-Barajneh", "Liban",	33.8483333,	35.50333333333333],
#   ["Akkar", "Liban",	34.5549465,	36.1950958],
#   ["Nahr El Bared", "Liban",	34.512250,	35.960167],
#   ["Ein-El-Hilweh", "Liban",	33,5396306,	35,37358056],
#   ["Chatila, Beyrouth", "Liban",	33.867482,	35.506979],
#   ["Dbayeh", "Liban",	33,9420159,	35,5937934],
#   ["Rashidieh", "Liban",	33.238279,	35.216018],
#   ["Beddawi", "Liban",	34,45,	35,8667],
#   ["Sabra, Beyrouth", "Liban",	33.870549,	35.501645],
#   ["Mar Lias", "Liban",	33,8959203,	35,47843],
#   ["Aida", "Cisjordanie",	31.7194444,	35.19888888888889],
#   ["Azraq", "Jordanie",	31.90528,	36.58083],
#   # ["Al-Amari", "Cisjordanie",	31,894056, 35,211533],
#   ["Tindouf", "Algérie", 	27.6666667,	-8.15],
#   ["Dheisheh", "Cisjordanie",	31.6938889,	35.18416666666666],
#   ["Rabouni", "Alégrie",	27.469785,	-8.088684],
#   ["Domiz", "Irak",	36.7823222,	42.89142361111111],
#   # ["Saricam", "Turquie",	371110882,	354731581],
#   ["Zaatari", "Jordanie",	32.2942395,	36.3267426],
#   ["Al Hol", "Syrie",	36.39082,	41.14663],
#   ["Mbera", "Mauritanie",	16.5652778,	-6.373611111111111],
#   ["Choucha", "Tunisie",	33.14,	11.46111111111111],
#   ["Yarmouk", "Syrie",	33.4733333,	36.305],
#   ["Makhmour", "Irak",	35.760820,	43.604718],
# ].each do |location, country, latitude, longitude|
#   Fiche.create!(pole_id: pole.id, country: country, location: location, latitude: latitude, longitude: longitude)
# end

# pole = Pole.create!(name: "Pôle Afrique")
# [
#   ["Mahama", "Rwanda", -2,2588075, 30,8225135],
#   ["Tropicana d'Avepozo", "Togo", 6.170132, 1.325044],
#   ["Intikane", "Niger", 16.1261111, 4.786944444444444],
#   ["Abala", "Niger", 14.9079444, 3.420361111111111],
#   ["Sayam Forage", "Niger", 13.6316667, 12.504444444444445],
#   ["Tabarey Barey", "Niger", 14.7362889, 0.9249499999999999],
#   ["Nguenyyiel", "Ethiopie", 8.2487778, 34.589333333333336],
#   ["Melkadia", "Ethiopie", 4.51937, 41.7201],
#   ["Kebribeyah", "Ethiopie", 9.2060, 42.4760],
#   ["Tierkidi", "Ethiopie", 8.2000000, 34.2666667],
#   ["Dolo Odo", "Ethiopie", 4.1666667, 42.06666666666667],
#   ["Bidi Bidi", "Ouganda" , 3.50984, 31.3099],
#   ["Kyangwali", "Ouganda", 1.220000, 30.820000],
#   ["Rhino", "Ouganda", 2.97279, 31.39633],
#   ["PTP", "Libéria", 6.0237931, -8.0562421],
#   ["Amboko", "Tchad", 7.9337889, 16.591099999999997],
#   ["Dosseye", "Tchad", 7.93617882, 16.5802401],
#   ["Iridimi", "Tchad", 15.116743198769106, 22.248989240000064],
#   ["Gondjé", "Tchad", 8.1436111, 16.521202777777777],
#   ["Minawao", "Cameroun", 10.5634556, 13.857175],
#   ["Gado-Badzere", "Cameroun", 5.75, 14.433],
#   ["Borgob", "Cameroun", 6.9274278, 14.821427],
#   ["Mbilé", "Cameroun", 4.2911111, 14.901002777777778],
#   ["Dzaleka", "Malawi", -13.6644703, 33.8691015],
#   ["Lusenda", "République démocratique du congo", -3.88333, 29.1],
#   ["Mole", "République démocratique du congo", 	4.3433333, 18.592777777777776],
#   ["Inke", "République démocratique du congo", 	4.089010, 20.766451],
#   ["Mulongwe", "République démocratique du congo", 	4.30241, 28.94197],
#   ["Tongogara", "Zimbabwé", -20.349785, 32.309918],
#   ["Kakuma", "Kenya", 3.753338, 34.825154],
#   ["Ampain", "Ghana", 5.295999, -2.295999],
#   ["Mugombwa", "Rwanda", -24.6755556, 29.850194444444448],
#   ["Nyarugusu", "Tanzanie", -3.1145267, 32.2358796],
#   ["Dadaab", "Kenya", 0.0530556, 40.308611111111105],
#   ["Luwani", "Malawi", -15.4805236, 34.7388553],
#   ["Krisan", "Ghana", 4.962690, -2.463503],
#   ["Kalma", "Darfour", 12.0125, 25.004167],
# ].each do |location, country, latitude, longitude|
#   Fiche.create!(pole_id: pole.id, country: country, location: location, latitude: latitude, longitude: longitude)
# end

# pole = Pole.create!(name: "Pôle Asie - Pacifique")
# [
#   ["Mae La", "Thaïlande", 17.1288889, 98.38055555555555],
#   ["Nu Po", "Thaïlande", 15.85, 98.66666666666667],
#   ["Tham Hin", "Thaïlande", 13.699378, 99.267118],
#   ["Kutapalong", "Bangladesh", 21.357222222222223, 92.08346999999999],
#   ["Shamlapur", "Bangladesh", 21.0780719, 92.1437284],
#   ["Leda", "Bangladesh", 20.9604, 92.2536],
#   ["Jadimura", "Bangladesh", 20.948223, 92.253660],
#   ["Unchiprang", "Bangladesh", 21.01285, 92.24968],
#   ["Niatak", "Iran", 29.492887, 60.850302],
#   ["Torbat e jam", "Iran", 35.2432546, 60.626313],
#   ["Loi je Lisu", "Kachin", 24.2493952, 97.3459136],
#   ["Gulan", "Afghanistan", 33.2874, 69.9128],
#   ["Shamshatoo", "Pakistan", 33.878652, 71.712453],
#   ["Kurasan", "Pakistan", 34.082190, 71.562169],
#   ["Ohn taw gyi Sud", "Myanmar", 20.185092, 92.802296],
#   ["Belgandi", "Népal", 26.6597, 87.6972],
#   ["Sanischare", "Népal", 26.656420, 87.550687],
# ].each do |location, country, latitude, longitude|
#   Fiche.create!(pole_id: pole.id, country: country, location: location, latitude: latitude, longitude: longitude)
# end


# pole = Pole.create!(name: "Pôle Europe")
# [
#   ["Brindisi", "Italie",	40.6330833, 17.872666666666667],
#   ["Lampedusa", "Italie",	35.5005, 12.6058],
#   ["Pian Del Lago", "Italie",	37.469008, 14.033805],
#   ["Messine", "Italie",	38.1923, 15.5555],
#   ["Trapani", "Italie",	38.017618, 12.537202],
#   ["Moria", "Gréce",	39,134893, 26,503508],
#   ["Leros", "Gréce",	37.1178333, 26.8599444],
#   ["Malakasa", "Gréce",	38.23890, 23.779277777777775],
#   ["Nea Kavala", "Grèce",	40.989804, 22.626153],
#   ["Vathy", "Gréce", 	37.762551, 26.9781802],
#   ["Vial", "Gréce",	38.192384, 26.5326],
#   ["Subotica", "Serbie",	46.1, 19.666666666666668],
#   ["Bosilegrad", "Serbie",	42.501742, 22.474249]
# ].each do |location, country, latitude, longitude|
#   Fiche.create!(pole_id: pole.id, country: country, location: location, latitude: latitude, longitude: longitude)
# end

# pole = Pole.create!(name: "Pôle Amériques")
# [
#   ["Buenos Aire", "Argentine", -34.6131500, -58.3772300],
#   ["Cucut", "Colombie", 	7.824047283515314	, 72.45818053925628],
#   ["Bogot", "Colombie", 	4.6768752	, 74.0889609],
#   ["Maica", "Colombie", 	11.3832	, 72.2367],
#   ["Quibd", "Colombie", 	5.6984062745548405	, 76.66400290171303],
#   ["La cru", "Costa Rica",	11.0615	, 85.414],
#   ["El Barreta", "Mexique", 	32.4866667	, 116.87702777777777],
#   ["Pacaraim", "Brésil", 	4.4308333	, 61.145833333333336],
#   ["Belmopa", "Bélize",  	17.333	, 88.8358],
#   ["Upal", "Costa Rica",	10.902677	, 85.015343],
#   ["Port Isabe", "USA",	26.069380373874623	, 97.20568814171685],
#   ["Golfit", "Costa Rica",	8.6241469	, 83.0658418],
#   ["Corail - Cesseles", "Haïti", 	18.653125	, 72.253987],
# ].each do |location, country, latitude, longitude|
#   Fiche.create!(pole_id: pole.id, country: country, location: location, latitude: latitude, longitude: longitude)
# end

# (1..10).each do |index|
#   Post.create!(name: Faker::Lorem.sentence, content: Faker::Lorem.paragraph(sentence_count: 4), for: :obs)
#   Post.create!(name: Faker::Lorem.sentence, content: Faker::Lorem.paragraph(sentence_count: 4), for: :camps)
#   Post.create!(name: Faker::Lorem.sentence, content: Faker::Lorem.paragraph(sentence_count: 4), for: :blog)
# end

Person.delete_all

[
  ["Jessica 	RAVARD ", 41],
  ["Imen	GUERBAA", 41],
  ["Ismaël 	ZEIN", 41],
  ["Marjorie 	HERNANDEZ", 41],
  ["Inès 	ZAKY", 41],
  ["Marie 	MATHIEU", 41],
  ["Jeanne 	Jeanne SARHAN", 41],
  ["Haciba 	Haciba MEFTAH", 41],
  ["Sarah 	TEFFENE", 41],
  ["Camille	RAILLON", 41],
  ["Rafaëlle 	BERTHAULT", 41],
  ["Georgio 	EL KHOURY", 41],
  ["Charlotte 	GUIBERT", 41],
  ["Antoinette 	REYRE ", 41],
  ["Clotilde 	GOUX", 41],
  ["Ozan 	DOGAN AVUNDUK", 41],
  ["Pola 	ANQUETIL-BARBA ", 41],
  ["Amandine	DESMONT", 41],
  ["Salima 	BAHIA", 41],
].each do |(name, pole_id)|
  Person.create!(name: name, pole_id: pole_id)
end

[
  ["Sarah 	PAYEN", 42],
  ["Jessica-Andrée 	SAHOU", 42],
  ["Jean-Claude 	KPINKIA", 42],
  ["Ibrahim 	ALI MADOUGOU", 42],
  ["Camélia 	 KOUMAS", 42],
  ["Khole 	DIOUF", 42],
  ["Pauline 	GAGLIARDINI", 42],
  ["Wilfried 	ADOU", 42],
  ["Marion 	ANGELINI", 42],
  ["Mariame 	SIDIBE", 42],
  ["Issagha 	DIALLO", 42],
  ["Manon 	VINCLAIRE", 42],
  ["Martial 	MANET", 42],
  ["Marion 	JOSE", 42],
  ["Guillain 	MUCHIKA", 42],
  ["Soraya 	SADOUDI", 42],
  ["Fiona 	LEMOINE", 42],
  ["Martial 	WAGANG NONO", 42],
  ["Elise 	RAMETTE", 42],
  ["S.	A.H.", 42],
  ["Luc	LERICHE", 42],
  ["Auriane	 ROCHOIS", 42],
  ["A.	U. ", 42],
  ["Solène 	YAOMBITI", 42],
  ["Bourahima 	KONKOBO", 42],
  ["Nandi 	EWANGO ", 42],
  ["Perla 	HAJJ", 42],
  ["Pierre 	MICHAUD ", 42],
].each do |(name, pole_id)|
  Person.create!(name: name, pole_id: pole_id)
end

[
  ['Nawel 	EL GHIATI', 42],
  ['Romane 	BONNEME ', 42],
  ['Peyman 	BAGHDADI ', 42],
  ['Esmma 	EL SAMALOUTY ', 42],
  ['Rima 	KHEMIRI ', 42],
  ['Isis	EMAM', 42],
  ['Adèle 	CROISE', 42],
  ['Louise-Anne 	BAUDRIER', 42],
  ['Myriam	SEFRAOUI ', 42],
  ['Pierre Marc	MARC', 42],
  ['Alexia 	DUBREU ', 42],
  ['Louise 	BRICHET', 42],
  ['O.	B.', 42],
  ['Galatée 	LAPEYRE', 42],
  ['Blandine	RICHARD', 42],
  ['Karen 	PETIT   ', 42],
].each do |(name, pole_id)|
  Person.create!(name: name, pole_id: pole_id)
end

[
  ['Louise 	AZZONI', 44],
  ['Mallaury 	LEMASSON', 44],
  ['Cécile 	QUEVAL', 44],
  ['Racha 	FAYED', 44],
  ['Camille 	MARTEL', 44],
  ['Gaëlle 	DOLL', 44],
  ['Louis	 FERNIER', 44],
  ['Marion 	DUMONTET', 44],
  ['Melissa 	LIMUKA', 44],
  ['Aurélie 	GRIES', 44],
  ['Argjend 	BERISHA', 44],
  ['Clémence 	Delpeux ', 44],
].each do |(name, pole_id)|
  Person.create!(name: name, pole_id: pole_id)
end

[
  ["Adèle 	CROISE", 45],
  ["Pierre-Valery 	BELIARD", 45],
  ["Lucie 	FOUCHIER", 45],
  ["Nejma 	BANKS", 45],
  ["Elise 	LAURIOT-PREVOST", 45],
  ["Anne-Laure 	BARBE", 45],
  ["Isabella 	LEROY", 45],
].each do |(name, pole_id)|
  Person.create!(name: name, pole_id: pole_id)
end
